<?php


// Dirección o IP del servidor MySQL
$host = "192.168.10.214";

// Puerto del servidor MySQL
$puerto = "3306";

// Nombre de usuario del servidor MySQL
$usuario = "german";

// Contraseña del usuario
$contrasena = "german";

// Nombre de la base de datos
$baseDeDatos = "prueba";

// Nombre de la tabla a trabajar
$tabla = "estres";

function Conectarse()
{
    global $host, $puerto, $usuario, $contrasena, $baseDeDatos, $tabla;

    if (!($link = mysqli_connect($host . ":" . $puerto, $usuario, $contrasena))) {
        echo "Error conectando a la base de datos.<br>";
        exit();
    } else {
        echo "Listo, estamos conectados.<br>";
    }
    if (!mysqli_select_db($link, $baseDeDatos)) {
        echo "Error seleccionando la base de datos.<br>";
        exit();
    } else {
        echo "Obtuvimos la base de datos $baseDeDatos sin problema.<br>";
    }
    return $link;
}


function generateRandomString($length = 10)
{
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

$link = Conectarse();
/*
for($i = 1;$i < 10;$i++){
    $sql = "INSERT INTO estres (campo1,campo2,campo3,campo4,campo5,campo6,campo7)VALUES (".
        "'".generateRandomString(2500)."',".
        "'".generateRandomString(2500)."',".
        "'".generateRandomString(2500)."',".
        "'".generateRandomString(2500)."',".
        "'".generateRandomString(2500)."',".
        "'".generateRandomString(2500)."',".
        "'".generateRandomString(2500)."');";

    if (mysqli_query($link, $sql)) {
        echo "New record created successfully";
    } else {
        echo "Error: " . $sql . "<br>" . mysqli_error($link);
        end;
    }

}
*/

for ($i = 1; $i < 10; $i++) {
    $sql = "INSERT INTO estres (campo1,campo2,campo3,campo4,campo5,campo6,campo7)VALUES (" .
        "'" . generateRandomString(2500) . "'," .
        "'" . generateRandomString(2500) . "'," .
        "'" . generateRandomString(2500) . "'," .
        "'" . generateRandomString(2500) . "'," .
        "'" . generateRandomString(2500) . "'," .
        "'" . generateRandomString(2500) . "'," .
        "'" . generateRandomString(2500) . "');";

    if (mysqli_query($link, $sql)) {
        echo "New record created successfully";
    } else {
        echo "Error: " . $sql . "<br>" . mysqli_error($link);
        end;
    }

}

echo 'FIN';




?>